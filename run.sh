#!/bin/bash


#./galsim 100 ../input_data/ellipse_N_01000.gal 2000 1e-5 0.02 $1
#../compare_gal_files/compare 1000 ../ref_output_data/ellipse_N_01000_after200steps.gal result.gal

particles=2000
timesteps=200

./galsim $particles ../input_data/ellipse_N_02000.gal $timesteps 1e-5 0.25 0
../compare_gal_files/compare $particles result.gal ../ref_output_data/ellipse_N_02000_after200steps.gal

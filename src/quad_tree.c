#include "quad_tree.h"
#include "vec2.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
//#define N_PREALLOC 400

//static Quadrouple *quad_pool;


void print_tree(Quadrouple *node, Particles* parts, int curr_depth)
{
    if(node ==NULL)
        return;

    for(int i = 0; i < curr_depth; i++)
        printf("- ");
    if(node->particle_id >= 0)
    {
        printf("Leaf  (%f mass units)\n", parts->mass[node->particle_id]);
    }
    else
    {
        printf("Node  (%f mass units)\n", node->mass_sum);
    }
    
    print_tree(node->nw, parts, curr_depth+1);
    print_tree(node->ne, parts, curr_depth+1);
    print_tree(node->sw, parts, curr_depth+1);
    print_tree(node->se, parts, curr_depth+1);
}

void free_quad_tree(Quadrouple *tree)
{
    if(tree == NULL)
        return;

    free_quad_tree(tree->ne);
    free_quad_tree(tree->nw);
    free_quad_tree(tree->se);
    free_quad_tree(tree->sw);

    free(tree);
}

static Quadrouple *create_quad(double x, double y, double width, double height)
{
    Quadrouple *quad = malloc(sizeof(Quadrouple));
    quad->width = width;
    quad->height = height;
    quad->pos = create_vec(x, y);
    quad->mass_sum = 0;
    quad->ne = NULL;
    quad->se = NULL;
    quad->nw = NULL;
    quad->sw = NULL;
    quad->particle_id = -1;
    quad->center_of_mass = create_vec(0, 0);
    quad->used = 1;

    return quad;
}

Quadrouple *create_quad_tree(double w, double h)
{
    //quad_pool = malloc(sizeof(Quadrouple) * N_PREALLOC);
    return create_quad(0, 0, w, h);
}

static int particle_in_quadrouple(Quadrouple *quad, Particles *parts, int id)
{
    double px = parts->posX[id];
    double py = parts->posY[id];
    if( (px >= quad->pos.x) && px <= (quad->pos.x + quad->width) && 
        (py >= quad->pos.y) && py <= (quad->pos.y + quad->height))
    {
        return 1;
    }
    else
    {
        return 0;
    }
}

Vec2 gravity_component(Quadrouple *node, Particles *parts, Vec2 pos, int otherId, double theta_max)
{
    Vec2 center, d, D, ret;
    double len, mass, thet;

    center = add_vec(node->pos, create_vec(node->width/2, node->height/2));
    d = sub_vec(center, pos);

    // Theta
    thet = node->width / length_vec(d);

    // If is non empty leaf
    if(node->ne == NULL)
    {
        if(node->particle_id < 0 || node->particle_id == otherId)
            return create_vec(0, 0);

        int i = node->particle_id;

        mass = parts->mass[i];

        D.x = parts->posX[i] - pos.x;
        D.y = parts->posY[i] - pos.y;

        len = length_vec(D) + epsilon;
       
        ret.x = (D.x * mass)/(len*len*len);
        ret.y = (D.y * mass)/(len*len*len);
        return ret;
    } 
    else if(thet < theta_max) // equivalent mass
    {
        mass = node->mass_sum;

        D = sub_vec(mult_vec(1/mass, node->center_of_mass),
                    create_vec(pos.x, pos.y));

        len = length_vec(D) + epsilon;
        ret = mult_vec(mass/(len*len*len), D);
        return ret;
    }
    return  add_vec( 
                add_vec(gravity_component(node->ne, parts, pos, otherId, theta_max),
                        gravity_component(node->se, parts, pos, otherId, theta_max)),
                add_vec(gravity_component(node->nw, parts, pos, otherId, theta_max),
                        gravity_component(node->sw, parts, pos, otherId, theta_max)));
}

static void subdivide_quad(Quadrouple *node)
{
    node->nw = create_quad(node->pos.x,               node->pos.y+node->height/2,   node->width/2, node->height/2);
    node->ne = create_quad(node->pos.x+node->width/2, node->pos.y+node->height/2,   node->width/2, node->height/2);
    node->sw = create_quad(node->pos.x,               node->pos.y,                  node->width/2, node->height/2);
    node->se = create_quad(node->pos.x+node->width/2, node->pos.y,                  node->width/2, node->height/2);
}

void add_particle(Quadrouple *node, Particles *parts, int particle_id)
{
    // if node exists and cell encapsulates particle
    if(particle_in_quadrouple(node, parts, particle_id))
    {
        // If quad is leaf node
        if(node->ne == NULL)
        {
            // If node contains no particle
            if(node->particle_id < 0)
            {
                // add particle to quad
                node->particle_id = particle_id;
            }
            else // If two particles in same quad
            {
                // subdivide
                subdivide_quad(node);

                // Add to cells center of mass and add traverse
                node->mass_sum += parts->mass[particle_id];
                node->center_of_mass.x += parts->mass[particle_id]*parts->posX[particle_id];
                node->center_of_mass.y += parts->mass[particle_id]*parts->posY[particle_id];

                node->mass_sum += parts->mass[node->particle_id];
                node->center_of_mass.x += parts->mass[node->particle_id]*parts->posX[node->particle_id];
                node->center_of_mass.y += parts->mass[node->particle_id]*parts->posY[node->particle_id];

                add_particle(node->nw, parts, node->particle_id);
                add_particle(node->ne, parts, node->particle_id);
                add_particle(node->sw, parts, node->particle_id);
                add_particle(node->se, parts, node->particle_id);

                node->particle_id = -1;

                add_particle(node->nw, parts, particle_id);
                add_particle(node->ne, parts, particle_id);
                add_particle(node->sw, parts, particle_id);
                add_particle(node->se, parts, particle_id);
            }
        }
        else
        {
            add_particle(node->nw, parts, particle_id);
            add_particle(node->ne, parts, particle_id);
            add_particle(node->sw, parts, particle_id);
            add_particle(node->se, parts, particle_id);

            // Add to cells center of mass and traverse down tree
            node->mass_sum += parts->mass[particle_id];
            node->center_of_mass.x += parts->mass[particle_id]*parts->posX[particle_id];
            node->center_of_mass.y += parts->mass[particle_id]*parts->posY[particle_id];   
        }
        
    }
}
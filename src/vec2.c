#include "vec2.h"

Vec2 sub_vec(Vec2 a, Vec2 b)
{
    Vec2 c;
    c.x = a.x - b.x;
    c.y = a.y - b.y;
    return c; 
}

Vec2 add_vec(Vec2 a, Vec2 b)
{
    Vec2 c;
    c.x = a.x + b.x;
    c.y = a.y + b.y;
    return c;
}

Vec2 mult_vec(double C, Vec2 a)
{
    Vec2 c;
    c.x = a.x*C;
    c.y = a.y*C;
    return c;
}

double dot_vec(Vec2 a, Vec2 b)
{
    return a.x * b.x + a.y * b.y;
}

double length_vec(Vec2 a)
{
    return sqrt(a.x*a.x + a.y*a.y);
}


Vec2 normalize_vec(Vec2 a)
{
    return mult_vec(1/length_vec(a), a);
}

Vec2 create_vec(double x, double y)
{
    Vec2 ret;
    ret.x = x;
    ret.y = y;
    return ret;
}
#pragma once
#include "math.h"

typedef struct Vec2_
{
    double x;
    double y;
} Vec2;


Vec2 create_vec(double x, double y);
Vec2 sub_vec(Vec2 a, Vec2 b);
Vec2 add_vec(Vec2 a, Vec2 b);
Vec2 mult_vec(double C, Vec2 a);
double dot_vec(Vec2 a, Vec2 b);
double length_vec(Vec2 a);
Vec2 normalize_vec(Vec2 a);
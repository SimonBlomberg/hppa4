#pragma once
#include "quad_tree.h"


typedef struct Quadrouple_t Quadrouple;
typedef struct Particles_t
{
    double *velX;
    double *velY;
    double *posX;
    double *posY;
    double *mass;
    double *brightness;
    int N_particles;
} Particles;


void allocate_particles(Particles *parts, int N);
void free_particles(Particles *parts);
void parse_galaxy_file(const char *filename, Particles *part);
void save_to_galaxy_file(const char *filename, Particles *parts);
void print_particle(int i, Particles *part);
void update_particle_forces(int i, Particles *part, double dt, Quadrouple *qTree, double max_theta);
void update_particle_positions(int i, Particles *part, double dt);